const { authJwt, uploadFile } = require('../middlewares')
const controller = require('../controllers/product.controller')
const multer = require('multer')
const path = require('path')

const storageImageProfile = multer.diskStorage({
  destination: 'E:/DoAn/da-ktmt/upload/images',
  filename: (req, file, cb) => {
    return cb(
      null,
      `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`,
    )
  },
})
const uploadImageProfile = multer({
  storage: storageImageProfile,
})

const storageQRcode = multer.diskStorage({
  destination: 'E:/DoAn/da-ktmt/upload/qr-code',
  filename: (req, file, cb) => {
    return cb(
      null,
      `${file.fieldname}_${Date.now()}${path.extname(file.originalname)}`,
    )
  },
})
const uploadQRcode = multer({
  storage: storageQRcode,
})

module.exports = function (app) {
  app.use(function (req, res, next) {
    res.header(
      'Access-Control-Allow-Headers',
      'x-access-token, Origin, Content-Type, Accept',
    )
    next()
  })
  app.post(
    '/api/product/createProduct',
    [authJwt.verifyToken, uploadImageProfile.single('profile')],
    controller.createProduct,
  )
  app.get(
    '/api/product/getListProduct',
    [authJwt.verifyToken],
    controller.getListProduct,
  )
  app.get(
    '/api/product/getListProductAdmin',
    [authJwt.verifyToken, authJwt.isAdmin],
    controller.getListProductAdmin,
  )
  app.get(
    '/api/product/detailProduct',
    [authJwt.verifyToken],
    controller.detailProduct,
  )
  app.delete(
    '/api/product/deleteProduct/:productId',
    [authJwt.verifyToken],
    controller.deleteProduct,
  )
  app.patch(
    '/api/product/updateProduct/:productId',
    [authJwt.verifyToken],
    controller.updateProduct,
  )
  app.post(
    '/api/product/updateHistoryProduct',
    [authJwt.verifyToken],
    controller.updateHistoryProduct,
  )
  app.post(
    '/api/product/addQrcodeProduct',
    [authJwt.verifyToken, uploadQRcode.single('qrcode')],
    controller.addQrcodeProduct,
  )
}
