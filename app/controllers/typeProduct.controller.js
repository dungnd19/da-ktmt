const config = require("../config/auth.config");
const db = require("../models");
const QRCode = require('qrcode');
const User = db.user;
const Role = db.role;
const Product = db.product;
const typeProduct = db.typeProduct;

exports.createTypeProduct = async(req, res) => {
    new typeProduct({
    name: req.body.name,
    description: req.body.description
    }).save((err, typeProduct) => {
        if (err) {
            res.status(500).json({
                success: false,
                message: `thêm loại không sản phẩm thành công`,
              });
        }
        else {
            res.status(200).json({
                success: true,
                message: `thêm loại sản phẩm thành công`,
              });
        }
    })
}
exports.getListTypeProduct = async(req, res) => {
    let page = parseInt(req.query['page'])
    let size = parseInt(req.query['size'])
    let skip = parseInt((page - 1) * size)
    typeProduct.find({
        isDeleted: false
    })
    .limit(size)
    .skip(skip)
    .then((typeProduct) => {
        return res.status(200).json({
        success: true,
        message: 'lây danh sách loại sản phầm thành công',
        page: page,
        size: size,
        typeProducts: typeProduct,
        });
    })
    
    .catch((err) => {
        res.status(500).json({
        success: false,
        message: 'Lấy danh sách loại sản phẩm không thành công',
        error: err.message,
        });
    });
}