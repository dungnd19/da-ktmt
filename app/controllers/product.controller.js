const config = require('../config/auth.config')
const db = require('../models')
const QRCode = require('qrcode')
const User = db.user
const Role = db.role
const Product = db.product
const typeProduct = db.typeProduct
var jwt = require('jsonwebtoken')
var bcrypt = require('bcryptjs')

exports.createProduct = async (req, res) => {
  // create_data = req.dataa;
  console.log(req.body['data'])
  create_data = JSON.parse(req.body['data'])
  console.log(create_data)
  var name = create_data['name']
  var origin = create_data['origin']

  const product = new Product({
    name: name,
    origin: origin,
    mass: create_data['mass'],
    massUnit: create_data['massUnit'],
    price: create_data['price'],
    imageUrl: `profile/${req.file.filename}`,
  })

  product.save((err, product) => {
    if (err) {
      console.log('aaaaaa', err)
      res.status(500).send({ message: err })
      return
    }
    User.find(
      {
        _id: req.userId,
      },
      (err, user) => {
        if (err) {
          res
            .status(500)
            .send({ status: false, message: 'Không xác định được người dùng' })
          return
        }
        product.users = user.map((user) => user._id)
        product.save((err) => {
          if (err) {
            res.status(500).send({ message: err })
            return
          }
          typeProduct.find(
            {
              _id: create_data['typeProductId'],
            },
            (err, typeProduct) => {
              if (err) {
                res
                  .status(500)
                  .send({
                    status: false,
                    message: 'Không xác định được loại sản phẩm',
                  })
                return
              }
              product.typeProduct = typeProduct.map(
                (typeProduct) => typeProduct._id,
              )
              product.save((err, productDetail) => {
                if (err) {
                  res.status(500).send({ message: err })
                  return
                }
                res
                  .status(200)
                  .send({
                    status: true,
                    product: productDetail,
                    message: 'tạo sản phẩm thành công',
                  })
                return
              })
            },
          )
        })
      },
    )
  })
}

exports.detailProduct = async (req, res) => {
  const id = req.query['id']
  Product.find({
    _id: id,
    isDeleted: false,
  })
    .then((singleProduct) => {
      res.status(200).json({
        success: true,
        message: `lấy thông tin chi tiết sản phẩm thành công`,
        Product: singleProduct,
      })
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: 'sản phẩm không tồn tại',
        error: err.message,
      })
    })
}

exports.getListProduct = async (req, res) => {
  let page = parseInt(req.query['page'])
  let size = parseInt(req.query['size'])
  let skip = parseInt((page - 1) * size)
  Product.find({
    users: req.userId,
    isDeleted: false,
  })
    .limit(size)
    .skip(skip)
    .then((Products) => {
      return res.status(200).json({
        success: true,
        message: 'lây danh sách sản phầm thành công',
        page: page,
        size: size,
        Products: Products,
      })
    })

    .catch((err) => {
      res.status(500).json({
        success: false,
        message: 'Lấy danh sách sản phẩm không thành công',
        error: err.message,
      })
    })
}

exports.getListProductAdmin = async (req, res) => {
  let page = parseInt(req.query['page'])
  let size = parseInt(req.query['size'])
  let skip = parseInt((page - 1) * size)
  Product.find({
    isDeleted: false,
  })
    .limit(size)
    .skip(skip)
    .then((Products) => {
      return res.status(200).json({
        success: true,
        message: 'lây danh sách sản phầm thành công',
        page: page,
        size: size,
        Products: Products,
      })
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: 'Lấy danh sách sản phẩm không thành công',
        error: err.message,
      })
    })
}

exports.deleteProduct = async (req, res) => {
  let id = req.params.productId
  Product.findByIdAndUpdate(id, { isDeleted: true })
    .exec()
    .then((singleProduct) => {
      res.status(200).json({
        success: true,
        message: 'Xóa sản phẩm thành công',
      })
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: 'Xóa sản phẩm không thành công',
      })
    })
}

exports.updateProduct = async (req, res) => {
  create_data = req.body
  var name = create_data['name']
  var origin = create_data['origin']

  let id = req.params.productId
  Product.findByIdAndUpdate(id, {
    name: create_data['name'],
    origin: create_data['origin'],
    mass: create_data['mass'],
    massUnit: create_data['massUnit'],
    price: create_data['price'],
    typeProduct: create_data['typeProduct'],
  })
    .exec()
    .then((singleProduct) => {
      res.status(200).json({
        success: true,
        message: 'Cập nhật sản phẩm thành công',
      })
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: 'Cập nhật sản phẩm không thành công',
      })
    })
}

exports.updateHistoryProduct = async (req, res) => {
  create_data = req.body
  let movingLocationInfo = create_data['movingLocationInfo']
  let id = create_data['product_id']
  Product.updateOne(
    { _id: id },
    { $push: { movingLocationInfo: movingLocationInfo } },
  ).exec((err, product) => {
    if (err) {
      res.status(500).send({ message: err })
      return
    }
    res.status(200).send({
      status: true,
      message: 'cập nhật thông tin lịch trình di chuyển sản phẩm thành công',
    })
    return
  })
}

exports.addQrcodeProduct = async (req, res) => {
  productId = req.body['id']
  console.log(productId)
  qrUrlProduct = `qr-code/${req.file.filename}`
  console.log(qrUrlProduct)
  Product.findByIdAndUpdate(productId, { qrUrl: qrUrlProduct })
    .exec()
    .then((singleProduct) => {
      res.status(200).json({
        success: true,
        message: 'Cập nhật sản phẩm thành công',
      })
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: 'Cập nhật sản phẩm không thành công',
      })
    })
}
