const config = require('../config/auth.config')
const db = require('../models')
const User = db.user
const Role = db.role

var jwt = require('jsonwebtoken')
var bcrypt = require('bcryptjs')

exports.getListUser = async (req, res) => {
  let page = parseInt(req.query['page'])
  let size = parseInt(req.query['size'])
  let skip = parseInt((page - 1) * size)
  User.find({
    isDeleted: false,
  })
    .limit(size)
    .skip(skip)
    .then((Users) => {
      return res.status(200).json({
        success: true,
        message: 'lây danh sách user thành công',
        page: page,
        size: size,
        Users: Users,
      })
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: 'Lấy danh sách user không thành công',
        error: err.message,
      })
    })
}

exports.allAccess = (req, res) => {
  res.status(200).send('Public Content.')
}

exports.userBoard = (req, res) => {
  res.status(200).send('User Content.')
}

exports.adminBoard = (req, res) => {
  res.status(200).send('Admin Content.')
}

exports.moderatorBoard = (req, res) => {
  res.status(200).send('Moderator Content.')
}
const detailUser = (req, res) => {
  var token = req.headers['x-access-token']
  if (token) {
    jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        return res.status(401).send({ message: 'Unauthorized!' })
      }
      req.userId = decoded.id
      User.findOne({
        _id: req.userId,
      })
        .populate('roles', '-__v')
        .exec((err, user) => {
          if (err) {
            res.status(500).send({ message: err })
            return
          }
          if (!user) {
            return res.status(404).send({ message: 'User Not found.' })
          }
          res.status(200).send({
            id: user._id,
            username: user.username,
            email: user.email,
            phoneNumber: user.phoneNumber,
            role: user.roles.name,
          })
          return
        })
    })
  }
}
exports.detailUser = detailUser

exports.deleteUser = async (req, res) => {
  let id = req.params.id
  User.findByIdAndUpdate(id, { isDeleted: true })
    .exec()
    .then((singleProduct) => {
      res.status(200).json({
        success: true,
        message: 'Xóa user thành công',
      })
    })
    .catch((err) => {
      res.status(500).json({
        success: false,
        message: 'Xóa user không thành công',
      })
    })
}
