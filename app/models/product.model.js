const mongoose = require("mongoose");

const Product = mongoose.model(
  "Product",
  new mongoose.Schema({
    name: String,
    mass: {
      type: String,
      required: true,
    },
    massUnit: {
      type: String,
      required: true,
    },
    price: String,
    origin: String,
    qrUrl: String,
    imageUrl: String,
    isDeleted: {
      type: Boolean,
      default: false
    },
    movingLocationInfo: [
    ],
    typeProduct:
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "typeProduct"
      },
    users: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
      }
    ]
  })
);

module.exports = Product;