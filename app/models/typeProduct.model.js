const mongoose = require("mongoose");

const typeProduct = mongoose.model(
  "typeProduct",
  new mongoose.Schema({
    name: {
        type: String,
        required: true,
      },
    description: String,
    isDeleted: {
        type: Boolean,
        default: false
      },
  })
);

module.exports = typeProduct;