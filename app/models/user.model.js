const mongoose = require("mongoose");

const User = mongoose.model(
  "User",
  new mongoose.Schema({
    username: String,
    phoneNumber: {
      type: String,
      required: true,
    },
    fullName: {
      type: String,
      required: true,
    },
    address: String,
    gender: {
      type: String,
      enum: ['male', 'female', 'other']
    },
    email: String,
    password: {
      type: String,
      required: true,
    },
    isDeleted: {
      type: Boolean,
      default: false
    },
    // tokens: [{
    //   token: {
    //       type: String,
    //       required: true
    //   }
    // }],
    roles:
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Role"
      },
    // products: [
    //   {
    //     type: mongoose.Schema.Types.ObjectId,
    //     ref: "Product"
    //   }
    // ]
  })
);

module.exports = User;